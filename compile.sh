#!/bin/bash

set -xe


CFLAGS="-Wall -Wextra -Wshadow"

g++ $CFLAGS -c projet.cpp

g++ $CFLAGS -c OutilsCreationImage.cpp

g++ OutilsCreationImage.o projet.o -o projet
