#include <cstdint>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <cmath>
#include "OutilsCreationImage.h"
using namespace std;

// --- déclaration des constantes ---

// Les couleurs

const uint32_t CCiel = 0xCACAAAFF, CTable = 0x338833FF, CBrillance = 0xFFFFFF00,
  CTransparant = 0xFFFFFF00, CBoule = 0x880000FF;//0x6495EDFF;
const uint32_t couleurs[] = {CCiel, CTable, CBrillance, CTransparant, CBoule};

// Les tailles
const long H = 400;
const long L = 400;
const long centreH = 2*H/3;
const long centreL = 2*L/5;
const double rayon = 70;
const double rayon2 = rayon*rayon;

const double rayon_diff = 0.2;

const double xc = centreH + 2.5 * rayon_diff*rayon;
const double yc = centreL + 2.5 * rayon_diff*rayon;
const double a  = rayon * (1 + rayon_diff);
const double a2 = a*a;
const double b  = rayon * (1 - rayon_diff);
const double b2 = b*b;
// La trigo
const double pi = 3.14159;
const double theta = -pi/3;
const double costheta = cos(theta);
const double costheta2 = costheta * costheta;
const double sintheta = sin(theta);
const double sintheta2 = sintheta * sintheta;

// Spécifique élipse
const double m1 = (costheta2/a2) + (sintheta2/b2);
const double m = costheta * sintheta * (1/a2 - 1/b2);
const double m2 = sintheta2/a2 + costheta2/b2;

// --- Les fonctions
float borne01(float bornea, float borneb, float value)
{
    return (value - bornea) / (borneb - bornea);
}

double Q(double x, double y)
{
  /* sous fonction de l'élipse*/
  double x2 = x * x;
  double y2 = y * y;
  return(m1*x2 + 2*m*x*y + m2*y2);
}

double f(double x, double y)
{
  /* prend en entrée deux nombre x et y
   * et vérifie que le point formé par ces
   * deux nombres est bien dans l'élipse 
   * formée par (a, b, (xc,yc)) */
  double p = m1*xc + m*yc;
  double q = m*xc + m2*yc;
  return Q(x,y) - 2 * (p*x + q*y) + Q(xc, yc) - 1;
}

uint32_t alpha_blending( const uint32_t p, const uint32_t s, float aph)//, const float alpha = 0xFF)
{
  /* prend deux couleurs et l'alpha demandé en entrée
   * ressort une couleurs fusionnée */
  int mult = 0xFF; 
  float alpha = aph;  // alpha pour mélangeateur de couleur
  float negalpha = 1 - alpha;
    
  uint32_t rp = p >> 24;  //extracting red
  uint32_t rs = s >> 24;
  uint32_t gp = (p >> 16) & mult; //green 
  uint32_t gs = (s >> 16) & mult;
  uint32_t bp = (p >> 8) & mult;  //blue
  uint32_t bs = (s >> 8) & mult;
  
  uint32_t rf = (int)(alpha * rp + negalpha * rs) << 24; // mixing the 2 colors
  uint32_t gf = (int)(alpha * gp + negalpha * gs) << 16;
  uint32_t bf = (int)(alpha * bp + negalpha * bs) << 8;
    
  uint32_t f = rf | gf | bf | mult; //adding every parts of the colors
    
  return f;
}

double sig (double number) {
  return (1/(1+exp(-number)));
}

int main(void){
  cout << "Création d'une image représantant boule de billard" << endl;

  // définition des variables
  uint32_t fond[H][L], boule[H][L], brillance[H][L], ombre[H][L];
  double coef = 0.01;
  string path = "images_creees/";
  string name = path + "projet.bmp";
  uint32_t fin[H][L];
 

  cout << "creation du fond ..." << endl;
  // matrice représentatn le fond de l'image
  for(int i = 0; i<H; i++){
    for(int j = 0; j<L; j++){
      if (i<H/4)
	fond[i][j] = couleurs[0];
      else
	fond[i][j] = couleurs[1];
    }
  }

  cout << "fait !"<< endl <<"creation de la boule ..." << endl;
  // matrice représentant la boule
  for(int i = 0; i<H; i++){
    for(int j = 0; j<L; j++){	
      double x = i - centreH;
      double y = j - centreL;
      double d2 = x*x +  y*y;
      if(d2 < rayon2)
	boule[i][j] = couleurs[4];
    }
  }
  
  //brillance de la boule
  cout <<"fait... \n creation de la brillance ... \n";    
    for (int i = 0 ; i < H ; ++i ){
    for (int j = 0 ; j < L ; ++j )
      {
	int x = i-centreH;
	int y = j-centreL;
	int r = i-centreH + rayon/2;
	int s = j-centreL + rayon/2;
	double d2   = r*r+s*s;
	double d2xy = x*x+y*y;
	// remplissage de la matrice de pixels
	// calcul du carré de la distance entre
	// le centre de l'image et le pixel à remplir
	uint32_t opacite = 255/exp(coef*d2);
	if (d2xy <= rayon2){
	  brillance[i][j] = couleurs[3] | opacite;
	  
	}
      }
    }
    
  cout << "fait !"<< endl << "création de l'ombre ..." << endl;  
  for(int i = 0; i<H; i++){
    for(int j = 0; j<L; j++){
      //if (f(i,j) <= 0) ombre[i][j] = 0x001100FF;
      //else{
	double val = -f(i,j);
	ombre[i][j] = alpha_blending(0x001100FF, fond[i][j], sig(val));
      //}
    }
  }
        printf("%d\n", ombre[400][200]);
  
  
  cout << "fait !"<< endl <<"fusion des images ..." << endl;	
  for ( int i = 0; i < H; i++){
    for( int j = 0; j < L; j++){
      boule[i][j] = alpha_blending(boule[i][j], ombre[i][j], (boule[i][j] & 0xFF) / 255.0f);
      fin[i][j]   = alpha_blending(brillance[i][j], boule[i][j], (brillance[i][j] & 0xFF) / 255.0f);
    }
  }
  printf("%d", brillance[3][4]);
  OutilsCreationImage::creeImage(name, fin, H);  
  cout << "image créée" << endl;
   
  return 0;
}
